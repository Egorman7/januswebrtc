package com.example.januswebrtc;

import android.content.Context;
import android.media.AudioManager;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.github.helloiampau.janus.generated.Bundle;
import com.github.helloiampau.janus.generated.JanusCommands;
import com.github.helloiampau.janus.generated.JanusData;
import com.github.helloiampau.janus.generated.JanusEvent;
import com.github.helloiampau.janus.generated.JanusPlugins;
import com.github.helloiampau.janusclientsdk.JanusConfImpl;

import org.webrtc.AudioTrack;
import org.webrtc.RendererCommon;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoTrack;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements JanusManager.JanusManagerListener, JanusItemsAdapter.OnDataClick {
    // WebRTC Surface Renderer
    private SurfaceViewRenderer renderer;

    // Connect layout
    private ConstraintLayout layoutConnect;
    private EditText inputConnect;
    private Button buttonConnect;

    // Progress bar
    private ProgressBar progressBar;

    // List layout
    private ConstraintLayout layoutList;
    private ListView listView;

    // Connected layout
    private Button buttonDisconnect;

    // Other fields
    private JanusManager janusManager = null;

    @Override
    protected void onResume() {
        super.onResume();
        if (janusManager == null) {
            janusManager = new JanusManager(this);
            janusManager.setListener(this);
        }
    }

    @Override
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void initViews() {
        renderer = findViewById(R.id.renderer);

        layoutConnect = findViewById(R.id.layout_connect);
        inputConnect = findViewById(R.id.connect_input);
        buttonConnect = findViewById(R.id.connect_button);

        progressBar = findViewById(R.id.progress_bar);

        listView = findViewById(R.id.list_view);
        layoutList = findViewById(R.id.layout_list);

        buttonDisconnect = findViewById(R.id.button_disconnect);

        initListeners();
    }

    private void initListeners() {
        buttonConnect.setOnClickListener((view) -> {
            layoutConnect.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            janusManager.setHost(inputConnect.getText().toString());
            janusManager.connect();

        });
        buttonDisconnect.setOnClickListener(view -> {
            progressBar.setVisibility(View.GONE);
            layoutList.setVisibility(View.GONE);
            buttonDisconnect.setVisibility(View.GONE);
            layoutConnect.setVisibility(View.VISIBLE);
            janusManager.disconnect();
        });
    }

    private boolean rendererInitialized = false;

    @Override
    public void remoteVideoTrackReceived(VideoTrack videoTrack) {
        if (videoTrack != null) {
            if (!rendererInitialized) {
                renderer.init(janusManager.getService().rootEgl().getEglBaseContext(), null);
                rendererInitialized = true;
            }
            renderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL);
            videoTrack.addSink(renderer);
        }
    }

    @Override
    public void remoteAudioTrackReceived(AudioTrack audioTrack) {
        if (audioTrack != null) {
            AudioManager audioManager = ((AudioManager) this.getSystemService(Context.AUDIO_SERVICE));
            audioManager.setMode(AudioManager.MODE_NORMAL);
//            audioManager.setSpeakerphoneOn(true);
        }
    }

    @Override
    public void listReceived(List<JanusData> dataList) {
        progressBar.setVisibility(View.GONE);
        layoutConnect.setVisibility(View.GONE);
        layoutList.setVisibility(View.VISIBLE);
        buttonDisconnect.setVisibility(View.VISIBLE);
        JanusItemsAdapter adapter = new JanusItemsAdapter(dataList);
        adapter.setListener(this);
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(JanusData data) {
        janusManager.selectStream(data.getInt("id", 1));
    }

    @Override
    public void onStatusChanged(String status) {
        if (status.equals(JanusManager.STATUS_READY)) {
            Bundle context = Bundle.create();
            janusManager.dispatch(JanusCommands.LIST, context);
        } else if (status.equals(JanusManager.STATUS_OFF) || status.equals(JanusManager.STATUS_CLOSED)) {
            renderer.release();
//            layoutConnect.setVisibility(View.VISIBLE);
        }
    }
}