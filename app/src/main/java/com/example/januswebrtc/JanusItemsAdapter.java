package com.example.januswebrtc;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.helloiampau.janus.generated.JanusData;

import java.util.ArrayList;
import java.util.List;

public class JanusItemsAdapter extends BaseAdapter {
    List<JanusData> data = new ArrayList<>();
    JanusItemsAdapter(List<JanusData> data){
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return bindView(data.get(position), convertView != null ? convertView :  LayoutInflater.from(parent.getContext()).inflate(android.R.layout.activity_list_item, parent, false));
    }

    private View bindView(JanusData data, View convertView){
        ((TextView) convertView.findViewById(android.R.id.text1)).setText(data.getString("description", ""));
        convertView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onClick(data);
            }
        });
        return convertView;
    }

    private OnDataClick listener = null;

    public void setListener(OnDataClick listener) {
        this.listener = listener;
    }

    public interface OnDataClick {
        void onClick(JanusData data);
    }
}
