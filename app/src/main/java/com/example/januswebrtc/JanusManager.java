package com.example.januswebrtc;

import androidx.appcompat.app.AppCompatActivity;

import com.github.helloiampau.janus.generated.Bundle;
import com.github.helloiampau.janus.generated.JanusCommands;
import com.github.helloiampau.janus.generated.JanusData;
import com.github.helloiampau.janus.generated.JanusEvent;

import org.webrtc.AudioTrack;
import org.webrtc.VideoTrack;

import java.util.List;

public class JanusManager implements JanusService.ServiceDelegate {
    public static final String STATUS_READY = "READY", STATUS_OFF = "OFF", STATUS_CLOSED = "CLOSED";

    private final AppCompatActivity activity;

    private JanusService service = null;
    private JanusManagerListener listener = null;

    private String host = "";

    public JanusManager(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void setHost(String host){
        this.host = host;
    }

    public void connect(){
        service = new JanusService(activity, this, JanusHelper.getJanusConfiguration(host));
        bindObservers();
        service.start();
    }

    public void disconnect(){
        if(service != null){
            service.stop();
        }
    }

    public void dispatch(String message, Bundle payload){
        service.dispatch(message, payload);
    }

    private void bindObservers(){
        if(service != null){
            service.remoteVideoTrackListener().observe(activity, (track) -> {
                if(listener != null){
                    listener.remoteVideoTrackReceived(track);
                }
            });
            service.remoteAudioTrackListener().observe(activity, (track) -> {
                if(listener != null) {
                    listener.remoteAudioTrackReceived(track);
                }
            });
            service.statusListener().observe(activity, (status) -> {
                if(listener != null){
                    listener.onStatusChanged(status);
                }
            });
        }
    }

    public void selectStream(Long id){
        Bundle payload = Bundle.create();
        payload.setInt("id", id);
        service.dispatch(JanusCommands.WATCH, payload);
    }

    @Override
    public void onEvent(JanusEvent event, Bundle payload) {
        JanusData data = event.data();
        if(isJanusList(data, payload)){
            activity.runOnUiThread(() -> {
                    if(listener != null){
                        listener.listReceived(data.getObject("plugindata").getObject("data").getList("list"));
                    }
            });
        }
    }

    private boolean isJanusList(JanusData data, Bundle payload){
        return data.getString("janus","").equals("success") && payload.getString("command", "").equals(JanusCommands.LIST);
    }

    public void setListener(JanusManagerListener listener) {
        this.listener = listener;
    }

    public JanusService getService() {
        return service;
    }

    public interface JanusManagerListener {
        void remoteVideoTrackReceived(VideoTrack videoTrack);
        void remoteAudioTrackReceived(AudioTrack audioTrack);
        void listReceived(List<JanusData> dataList);
        void onStatusChanged(String status);
    }
}
