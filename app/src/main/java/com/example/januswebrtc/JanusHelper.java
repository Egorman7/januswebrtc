package com.example.januswebrtc;

import com.github.helloiampau.janus.generated.JanusPlugins;
import com.github.helloiampau.janusclientsdk.JanusConfImpl;

public class JanusHelper {
    public static JanusConfImpl getJanusConfiguration(String host){
        JanusConfImpl conf = new JanusConfImpl();
        conf.url(host);
        conf.plugin(JanusPlugins.STREAMING);
        return conf;
    }
}
